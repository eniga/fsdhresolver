﻿using FSDHResolver.Repositories.Dangote;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MyRow = FSDHResolver.Models.Dangote.AreasContext;

namespace FSDHResolver.Services.Dangote
{
    public class AreaService
    {
        AreaRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DangoteConnection"].ConnectionString;

        public AreaService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new AreaRepository(conn, generator);
        }

        public async Task<IEnumerable<MyRow>> GetAllAsync()
        {
            return await repo.FindAllAsync();
        }
    }
}