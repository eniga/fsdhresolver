﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FSDHResolver.Models.Dangote
{
    [Table("tbl_regions")]
    public class RegionsContext
    {
        [Key, Identity]
        public int Id { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        public string region_name { get; set; }
    }
}