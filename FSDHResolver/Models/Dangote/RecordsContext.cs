﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FSDHResolver.Models.Dangote
{
    [Table("tbl_records")]
    public class RecordsContext
    {
        [Key, Identity]
        public int Id { get; set; }
        public string company_code { get; set; }
        public string sales_organisation { get; set; }
        public string sales_organisation_description { get; set; }
        public string plant { get; set; }
        public string plant_description { get; set; }
        public string material { get; set; }
        public string material_description { get; set; }
        public string sales_unit { get; set; }
        public string Base_unit { get; set; }
        public string division { get; set; }
    }
}