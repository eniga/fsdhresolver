﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FSDHResolver.Models.Dangote
{
    [Table("tbl_areas")]
    public class AreasContext
    {
        [Key, Identity]
        public int Id { get; set; }
        public string company_code { get; set; }
        public string cbn_code { get; set; }
        public string credit_area { get; set; }
    }
}