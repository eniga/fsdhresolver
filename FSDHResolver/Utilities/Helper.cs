﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace FSDHResolver.Utilities
{
    public static class Helper
    {
        public static Serilog.Core.Logger LogConfig()
        {
            string SeqServerUrl = ConfigurationManager.AppSettings["SeqServerUrl"];
            string log = ConfigurationManager.AppSettings["log"];

            var logger = new LoggerConfiguration()
                .WriteTo
                .RollingFile(log, retainedFileCountLimit: 120)
                .WriteTo.Seq(SeqServerUrl)
                .CreateLogger();
            return logger;
        }

        public static bool IsNumeric(string value)
        {
            bool ok = value.All(Char.IsNumber);
            return ok;
        }
        public static string ObjectToXml(object obj)
        {
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = Encoding.GetEncoding("UTF-16")
            };

            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(String.Empty, String.Empty);

            var serializer = new XmlSerializer(obj.GetType());

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, settings))
                {
                    serializer.Serialize(xmlWriter, obj, namespaces);
                }
                return stringWriter.ToString();
            }
        }

        public static string ObjectToJson(object obj)
        {
            return obj.ToString();
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9.,]+/", "", RegexOptions.Compiled);
        }
    }
}