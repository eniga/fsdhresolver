﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace FSDHResolver.Controllers
{
    [RoutePrefix("api/Token")]
    public class TokenController : ApiController
    {
        TokenService.ServiceSoapClient service = new TokenService.ServiceSoapClient();

        [HttpPost]
        [Route("assignedtokenusage")]
        public async Task<TokenService.AssignedTokenUsageResponse> AssignedTokenUsage([FromBody]TokenService.AssignedTokenUsageRequestBody request)
        {
            var result = await service.AssignedTokenUsageAsync(request.serialno);
            return result;
        }

        [HttpGet]
        [Route("checkuser/{userid}")]
        public async Task<TokenService.CheckUserResponse> CheckUser(TokenService.CheckUserRequestBody request)
        {
            var result = await service.CheckUserAsync(request.userid);
            return result;
        }

        [HttpPut]
        [Route("disabletoken/{serialNo}")]
        public async Task<TokenService.DisableTokenResponse> DisableToken(string serialNo)
        {
            var result = await service.DisableTokenAsync(serialNo);
            return result;
        }

        [HttpPost]
        [Route("enabletoken")]
        public async Task<TokenService.EnableTokenResponse> EnableToken(TokenService.EnableTokenRequestBody request)
        {
            var result = await service.EnableTokenAsync(request.serialNo);
            return result;
        }

        [HttpPut]
        [Route("getactivationcode/{serialno}")]
        public async Task<TokenService.GetActivationCodeResponse> GetActivationCodeAsync(string serialno)
        {
            var result = await service.GetActivationCodeAsync(serialno);
            return result;
        }

        [HttpPost]
        [Route("getactivationpassword/{userName}")]
        public async Task<TokenService.GetActivationPasswordResponse> GetActivationPassword(string userName)
        {
            var result = await service.GetActivationPasswordAsync(userName);
            return result;
        }

        [HttpGet]
        [Route("getauthcode/{userid}")]
        public async Task<TokenService.GetAuthCodeResponse> GetAuthCode(string userid)
        {
            var result = await service.GetAuthCodeAsync(userid);
            return result;
        }

        [HttpGet]
        [Route("getreactivationcode/{serialno}")]
        public async Task<TokenService.GetReactivationCodeResponse> GetReactivationCode(string serialno)
        {
            var result = await service.GetReactivationCodeAsync(serialno);
            return result;
        }

        [HttpPut]
        [Route("syncusertoken/{serialno}")]
        public async Task<TokenService.SyncUserTokenResponse> SyncUserToken(string serialno)
        {
            var result = await service.SyncUserTokenAsync(serialno);
            return result;
        }

        [HttpPost]
        [Route("tokenassignment")]
        public async Task<TokenService.TokenAssignmentResponse> TokenAssignment([FromBody]TokenService.TokenAssignmentRequestBody request)
        {
            var result = await service.TokenAssignmentAsync(request.acctId, request.fullname, request.email, request.phone, request.tokenType, request.serialNo, request.expireDt);
            return result;
        }

        [HttpGet]
        [Route("tokenexist/{serialno}")]
        public async Task<TokenService.TokenExistResponse> TokenExist(string serialno)
        {
            var result = await service.TokenExistAsync(serialno);
            return result;
        }

        [HttpPost]
        [Route("tokenvalidator")]
        public async Task<TokenService.TokenValidatorResponse> TokenValidator([FromBody]TokenService.TokenValidatorRequestBody request)
        {
            var result = await service.TokenValidatorAsync(request.userid, request.otp);
            return result;
        }

        [HttpPut]
        [Route("unassigntoken/{serialNo}")]
        public async Task<TokenService.UnassignTokenResponse> UnassignToken(string serialNo)
        {
            var result = await service.UnassignTokenAsync(serialNo);
            return result;
        }

        [HttpPost]
        [Route("unlockusertoken")]
        public async Task<TokenService.UnlockUserTokenResponse> UnlockUserToken([FromBody]TokenService.UnlockUserTokenRequestBody request)
        {
            var result = await service.UnlockUserTokenAsync(request.userid, request.serialno);
            return result;
        }
    }
}