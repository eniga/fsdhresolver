﻿using FSDHResolver.FlexConnectService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FSDHResolver.Controllers
{
    [RoutePrefix("api/FlexConnect")]
    public class FlexConnectController : ApiController
    {
        private FlexConnectSoapClient service = new FlexConnectSoapClient();

        [HttpGet]
        [Route("balance/{AccountNumber}")]
        public FlexAccount BalanceEnq(string AccountNumber)
        {
            var result = service.BalanceEnq(AccountNumber);
            return result;
        }

        [HttpGet]
        [Route("balance/bvn/{AccountNumber}")]
        public FlexAccount BalanceEnqBVN(string AccountNumber)
        {
            var result = service.BalanceEnqBVN(AccountNumber);
            return result;
        }

        [HttpPost]
        [Route("fundtransfer")]
        public StatCode FundTransfer([FromBody]FundTransferRequest request)
        {
            var req = request.Body;
            var result = service.FundTransfer(req.TransRef, req.DebitAccount, req.CreditAccount, req.CurrencyCode, req.amount, req.TrnDate, req.ValueDate, req.Naration, req.Channel, req.TransCode, req.SourceCode, req.UserID, req.isCharge);
            return result;
        }

        [HttpGet]
        [Route("fundtransfer/tsq/{TransRef}")]
        public StatCode TranStatEnq(string TransRef)
        {
            var result = service.TranStatEnq(TransRef);
            return result;
        }

    }
}