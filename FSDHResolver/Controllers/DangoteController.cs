﻿using FSDHResolver.Models.Dangote;
using FSDHResolver.Services.Dangote;
using FSDHResolver.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace FSDHResolver.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Dangote")]
    public class DangoteController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();

        [HttpGet]
        [Route("authentication")]
        public DangoteAuthService.si_full_auth_absResponse Authentication()
        {
            var result = new DangoteAuthService.si_full_auth_absResponse();
            try
            {
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];
                
                DangoteAuthService.dt_full_auth_in request = new DangoteAuthService.dt_full_auth_in()
                {
                    full_synch = null,
                    password = password,
                    username = username
                };
                logger.Information("Dangote Authentication:: request - " + Helper.ObjectToXml(request));
                DangoteAuthService.si_full_auth_absClient service = new DangoteAuthService.si_full_auth_absClient();

                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                result = service.si_full_auth_absAsync(request).GetAwaiter().GetResult();
                logger.Information("Dangote Authentication:: response - " + Helper.ObjectToXml(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote Authentication:: " + ex.Message);
                throw new Exception(ex.Message);
            }
            return result;
        }

        [HttpGet]
        [Route("token")]
        public string GetToken()
        {
            string token = string.Empty;
            try
            {
                var response = Authentication();
                if (response.GetType() == typeof(DangoteAuthService.si_full_auth_absResponse))
                {
                    var result = response.mt_full_auth_out;
                    if (result.error_log.Length < 1)
                        token = result.authtoken;
                }

            }
            catch (Exception ex)
            {
                logger.Error("Dangote GetToken:: " + ex.Message);
                throw new Exception(ex.Message);
            }
            return token;
        }

        [HttpPost]
        [Route("passwordreset")]
        public async Task<DangotePasswordResetService.si_pass_reset_absResponse> PasswordReset([FromBody]DangotePasswordResetService.dt_pass_rest_in request)
        {
            logger.Information("Dangote Authentication:: request - " + Helper.ObjectToXml(request));
            DangotePasswordResetService.si_pass_reset_absClient service = new DangotePasswordResetService.si_pass_reset_absClient();

            // Set credentials
            string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
            string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

            service.ClientCredentials.UserName.UserName = baseAuthusername;
            service.ClientCredentials.UserName.Password = baseAuthpassword;
            service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

            var result = await service.si_pass_reset_absAsync(request);
            logger.Information("Dangote Authentication:: response - " + Helper.ObjectToXml(result));
            return result;
        }

        [HttpPost]
        [Route("customerdetails")]
        public async Task<DangoteGetCustomerService.si_atm_cust_abs_syncResponse> GetCustomerDetails([FromBody]DangoteGetCustomerService.dt_atm_cust_in request)
        {
            var result = new DangoteGetCustomerService.si_atm_cust_abs_syncResponse();
            logger.Information("Dangote Customer details:: request - " + Helper.ObjectToXml(request));
            DangoteGetCustomerService.si_atm_cust_abs_syncClient service = new DangoteGetCustomerService.si_atm_cust_abs_syncClient();
            try
            {
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                string token = GetToken();
                request.access_token = token;

                result = await service.si_atm_cust_abs_syncAsync(request);
                logger.Information("Dangote Customer details:: response - " + Helper.ObjectToXml(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote Customer details:: " + ex.Message);
            }
            return result;
        }

        [HttpPost]
        [Route("postpayment")]
        public async Task<DangotePostPaymentService.si_bank_abs_syc_v2Response> PostPayment([FromBody]DangotePostPaymentService.dt_bank_in request)
        {
            var result = new DangotePostPaymentService.si_bank_abs_syc_v2Response();
            logger.Information("Dangote postpayment:: request - " + Helper.ObjectToXml(request));
            DangotePostPaymentService.si_bank_abs_syc_v2Client service = new DangotePostPaymentService.si_bank_abs_syc_v2Client();
            try
            {
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                string token = GetToken();
                request.access_token = token;

                result = await service.si_bank_abs_syc_v2Async(request);
                logger.Information("Dangote postpayment:: response - " + Helper.ObjectToXml(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote postpayment:: " + ex.Message);
            }
            return result;
        }

        [HttpPost]
        [Route("getprice")]
        public async Task<DangotePriceService.si_price_abs_syncResponse> GetPrice([FromBody]DangotePriceService.dt_price_in request)
        {
            var result = new DangotePriceService.si_price_abs_syncResponse();
            logger.Information("Dangote getprice:: request - " + Helper.ObjectToXml(request));
            DangotePriceService.si_price_abs_syncClient service = new DangotePriceService.si_price_abs_syncClient();
            try
            {
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                string token = GetToken();
                request.acess_token = token;

                result = await service.si_price_abs_syncAsync(request);
                logger.Information("Dangote getprice:: response - " + Helper.ObjectToXml(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote getprice:: " + ex.Message);
            }
            return result;
        }

        [HttpPost]
        [Route("createatc")]
        public async Task<DangoteATCService.si_atm_dp_bapi_abs_syncResponse> CreateATCPayment([FromBody]DangoteATCService.dt_atm_dp_in request)
        {
            var result = new DangoteATCService.si_atm_dp_bapi_abs_syncResponse();
            //var result = new DangoteATCService.dt_doc_out();
            logger.Information("Dangote createatc:: request - " + Helper.ObjectToXml(request));
            DangoteATCService.si_atm_dp_bapi_abs_syncClient service = new DangoteATCService.si_atm_dp_bapi_abs_syncClient();
            try
            {
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                string token = GetToken();
                request.access_token = token;

                result = await service.si_atm_dp_bapi_abs_syncAsync(request);
                //result = service.si_atm_dp_bapi_abs_sync(request);
                logger.Information("Dangote createatc:: response; - " + Helper.ObjectToXml(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote createatc:: " + ex.Message);
            }
            return result;
        }

        [HttpGet]
        [Route("areas")]
        public async Task<IEnumerable<AreasContext>> GetAreas()
        {
            AreaService service = new AreaService();
            try
            {
                return await service.GetAllAsync();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote areas:: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        [Route("records")]
        public async Task<IEnumerable<RecordsContext>> GetRecords()
        {
            RecordService service = new RecordService();
            try
            {
                return await service.GetAllAsync();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote areas:: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        [Route("regions")]
        public async Task<IEnumerable<RegionsContext>> GetRegions()
        {
            RegionService service = new RegionService();
            try
            {
                return await service.GetAllAsync();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote areas:: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}